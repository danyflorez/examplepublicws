/** This publisher code and the RandService code are packaged, for convenience,
    in RandPublish.jar: 

    java -jar RandPublish.jar
*/

package com.ws.prove;

import javax.xml.ws.Endpoint;                                               /** line 1 **/

public class RandPublisher {                                                /** line 2 **/
    public static void main(String[ ] args) {
		final String url = "http://localhost:9300/rs";                      /** line 3 **/
		System.out.println("Publishing RandService at endpoint " + url);
	
		Endpoint.publish(url, new RandService());                           /** line 4 **/
    }
}
