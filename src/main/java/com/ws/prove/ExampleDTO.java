package com.ws.prove;

public class ExampleDTO {
	String nombre;
	String apellido;
	int numeroTarjetas;
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public int getNumeroTarjetas() {
		return numeroTarjetas;
	}
	public void setNumeroTarjetas(int numeroTarjetas) {
		this.numeroTarjetas = numeroTarjetas;
	}
	
	
}
